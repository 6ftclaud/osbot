import org.osbot.rs07.api.map.constants.Banks;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.map.Area;
import org.osbot.rs07.utility.ConditionalSleep;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;

@ScriptManifest(info = "Al Kharid mining", logo = "", version = 1.0, author = "6ftClaud", name = "Mining-AlKharid")

public class alkharid extends Script {
    private final int[] rocks = {11365, 11364};

    @Override
    public int onLoop() throws InterruptedException {
        Travel();
        Mine();
        return random(100, 200);
    }

    public void Mine() {
        RS2Object rockObject = getObjects().closest(getMineArea(), rocks);
        if(rockObject == null){
            log("Can't find any ores");
        }
        else
        {
            try {
                rockObject.interact("Mine");sleep(1200);
                while (myPlayer().isMoving() || myPlayer().isAnimating()) {
                    sleep(600);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void Travel() throws InterruptedException {
        settings.setRunning(true);
        if (getInventory().isFull()) {
            if (!Banks.AL_KHARID.contains(myPlayer())) getWalking().webWalk(Banks.AL_KHARID);
            else {
                if (!getBank().isOpen()) {
                    getBank().open();
                } else {
                    getBank().depositAllExcept(pickaxes -> pickaxes.getName().contains(" pickaxe"));
                }
            }
        } else {
            if (!getMineArea().contains(myPlayer())) getWalking().webWalk(getMineArea());
        }
    }

    public Area getMineArea() {
        return new Area(3294, 3308, 3296, 3312);
    }
}