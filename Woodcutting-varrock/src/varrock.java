import org.osbot.rs07.api.map.Area;
import org.osbot.rs07.api.map.constants.Banks;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;

@ScriptManifest(name = "Woodcutting-Varrock", version = 1.0, author = "HeyImJamie", info = "", logo = "")
public class varrock extends Script {

    @Override
    public int onLoop() throws InterruptedException {
        DismissRandom();
        if (shouldBank()) {
            bank();
        } else {
            cutTree(getTreeName());
        }
        return random(100, 200);
    }

    private boolean shouldBank() {
        return getInventory().isFull();
    }

    private void bank() throws InterruptedException {
        if (!Banks.VARROCK_WEST.contains(myPlayer())) {
            log("bank: walking to Western Varrock Bank");
            getWalking().webWalk(Banks.VARROCK_WEST);
        } else {
            if (!getBank().isOpen()) {
                log("bank: Opening bank");
                getBank().open();
            } else {
                log("bank: Depositing all items");
                getBank().depositAllExcept(axes -> axes.getName().contains(" axe"));
            }
        }
    }

    private void cutTree(String treeName) {
        if (!getTreeArea().contains(myPlayer())) {
            log("cutTree: walking to tree area");
            getWalking().webWalk(getTreeArea());
        } else {
            RS2Object tree = getObjects().closest(getTreeArea(), treeName);
            if (tree != null) {
                log("cutTree: Chopping down " + tree);
                tree.interact("Chop down");
                while (myPlayer().isAnimating() || myPlayer().isMoving()) ;
            }
        }
    }

    private Area getTreeArea() {
        if (getSkills().getDynamic(Skill.WOODCUTTING) >= 60) {
            return new Area(3203, 3506, 3225, 3497);
        } else {
            return new Area(3171, 3417, 3159, 3399);
        }
    }

    private String getTreeName() {
        if (getSkills().getDynamic(Skill.WOODCUTTING) >= 60) {
            return "Yew";
        } else if (getSkills().getDynamic(Skill.WOODCUTTING) >= 15) {
            return "Oak";
        } else {
            return "Tree";
        }
    }

    private void DismissRandom() {
        String[] NPClist = {"Bee keeper", "Capt' Arnav", "Niles", "Miles", "Giles", "Sergeant Damien", "Drunken dwarf",
                "Evil Bob", "Servant", "Postie Pete", "Molly", "Freaky Forester", "Genie", "Leo", "Dr. Jekyll",
                "Frogs", "Prince", "Princess", "Mysterious Old Man", "Pillory Guard", "Flippa", "Tilt", "Prison Pete",
                "Quiz Master", "Rick Turpentine", "Sandwich Lady", "Strange plant", "Dunce", "Mr. Mordaut"};
        NPC random = getNpcs().closest(NPClist);
        if (random != null) {
            log("DismissRandom: Random event occured, dismissing");
            random.interact("Dismiss");
            while (myPlayer().isMoving() || myPlayer().isInteracting(random)) ;
        }
    }
}
