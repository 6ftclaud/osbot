import org.osbot.rs07.api.Bank;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;

import java.time.Instant;

@ScriptManifest(info = "Combat in Lumbridge Swamp", logo = "", version = 1.0, author = "6ftClaud", name = "Combat-lumbridge_swamp")

public class lumbridge_swamp extends Script {
    //    private final Position location = new Position(3230, 3297, 0);
    private final Position location = new Position(3199, 3175, 0);
    private final Position bank = new Position(3208, 3220, 2);
    private final String food = "Anchovy pizza";
    private final int[] enemyIds = {8700, 8701};
    private final String loot = "Big bones";

    @Override
    public void onStart() throws InterruptedException {
        log("Starting script");
        if (HasFood()) Travel(location);
    }

    @Override
    public int onLoop() throws InterruptedException {
        if (!HasFood()) {
            Travel(bank);
            objects.closest("Bank booth").interact("Bank");
            try {
                sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Bank b = new Bank();
            b.withdrawAll(food);
            b = null;
            Travel(location);
        } else if (ReadyToAttack()) Attack();
        return 100;
    }

    public boolean HasFood() {
        if (getInventory().contains(food)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean ReadyToAttack() {
        if (!HasFood()) {
            return false;
        } else if (getCombat().isFighting()) {
            return false;
        } else if (myPlayer().isAnimating() || myPlayer().isUnderAttack() || myPlayer().isMoving()) {
            return false;
        } else {
            log("Ready to attack");
            return true;
        }

    }

    public void Attack() {
        NPC enemy = getNpcs().closest(enemyIds);

        if (enemy != null && !enemy.isUnderAttack()) {
            log("Attacking " + enemy);
            enemy.interact("Attack");
            long unixTimestampStart = Instant.now().getEpochSecond();
            while (enemy.exists()) {
                if (GetCurrentHealth() < 35) {
                    log("Low health, healing");
                    Heal();
                }
                if (Instant.now().getEpochSecond() - unixTimestampStart > 10 && !enemy.isUnderAttack()) {
                    log("Cutoff time elapsed, cancelling attack.");
                    break;
                }
            }
            // Collect loot and bury bones if possible
            try {
                sleep(600);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            GroundItem item = getGroundItems().closest("Big bones", "Bones", "Coins");
            if (item != null) {
                log("Picking up " + item + " off the ground");
                item.interact("Take");
                try {
                    sleep(1200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                getInventory().getItem("Big bones", "Bones").interact("Bury");
            }
        } else if (enemy == null) {
            Travel(location);
        }
    }

    public int GetCurrentHealth() {
        float maxHP = getSkills().getStatic(Skill.HITPOINTS);
        float currHP = getSkills().getDynamic(Skill.HITPOINTS);
        return (int) (100 * (currHP / maxHP));
    }

    public void Heal() {
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        getInventory().getItem(food).interact("Eat");
    }

    public void Travel(Position pos) {
        log("Traveling to " + pos);
        settings.setRunning(true);
        walking.walk(pos);
    }
}