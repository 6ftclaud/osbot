import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.MagicSpell;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.api.Bank;
import org.osbot.rs07.api.Magic;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;
import java.time.Instant;

@ScriptManifest(info = "Combat-varrock_dungeon", logo = "", version = 1.0, author = "6ftClaud", name = "Combat-varrock_dungeon")

public class varrock_dungeon extends Script{
    private final Position entrance = new Position(3115, 3449, 0);
    private final Position safespot = new Position(3123, 9849, 0);
    private final Position bank = new Position(3185, 3436, 0);
    private final String food = "Lobster";
    private final int [] enemyIds = {2098, 2099, 2100, 2101};

    @Override
    public void onStart() throws InterruptedException {
        log("Starting script");
        if(HasFood()) {
            Travel(entrance);
            RS2Object door = getObjects().closest("Door");
            if (map.canReach(door)) door.interact("Open");sleep(1200);
            RS2Object ladder = getObjects().closest("Ladder");
            if (map.canReach(ladder)) ladder.interact("Climb-down");sleep(1200);
            Travel(safespot);
        }
    }

    @Override
    public int onLoop() throws InterruptedException {
        if(!HasFood()){
            Travel(safespot);
//            Magic m = new Magic();
//            m.castSpell("Lumbridge Home Teleport");
//            Travel(bank);
//            objects.closest("Bank booth").interact("Bank");
//            try {
//                sleep(2000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            Bank b = new Bank();
//            b.withdrawAll(food);b = null;
//            Travel(entrance);
//            RS2Object door = getObjects().closest("Door");
//            if (map.canReach(door)) door.interact("Open");sleep(1200);
//            RS2Object ladder = getObjects().closest("Ladder");
//            if (map.canReach(ladder)) ladder.interact("Climb-down");sleep(1200);
//            Travel(safespot);
        }
        else if(ReadyToAttack()) Attack();
        return 100;
    }

    public boolean HasFood(){
        if(getInventory().contains(food)){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean ReadyToAttack(){
        if(!HasFood()){
            return false;
        }

        else if(getCombat().isFighting()){
            return false;
        }

        else if(myPlayer().isAnimating() || myPlayer().isUnderAttack() || myPlayer().isMoving()){
            return false;
        }

        else {
            log("Ready to attack");
            return true;
        }

    }

    public void Attack(){
        NPC enemy = getNpcs().closest(enemyIds);

        if(enemy != null && !enemy.isUnderAttack()){
            log("Attacking " + enemy);
            enemy.interact("Attack");
            long unixTimestampStart = Instant.now().getEpochSecond();
            while(enemy.exists()) {
                if (GetCurrentHealth() < 50) {
                    log("Low health, healing");
                    Heal();
                }
                if (Instant.now().getEpochSecond() - unixTimestampStart > 10 && !enemy.isUnderAttack()) {
                    log("Cutoff time elapsed, cancelling attack.");
                    break;
                }
            }
            // Collect loot and bury bones if possible
            try {
                sleep(600);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            GroundItem item = getGroundItems().closest("Big bones", "Bones", "Coins");
            if(item != null){
                log("Picking up " + item + " off the ground");
                item.interact("Take");
                try {
                    sleep(1200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                getInventory().getItem("Big bones", "Bones").interact("Bury");
            }
        }
        else if(enemy == null){
            Travel(safespot);
        }
    }

    public int GetCurrentHealth(){
        float maxHP = getSkills().getStatic(Skill.HITPOINTS);
        float currHP = getSkills().getDynamic(Skill.HITPOINTS);
        return (int) (100 * (currHP / maxHP));
    }

    public void Heal() {
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        getInventory().getItem(food).interact("Eat");
    }

    public void Travel(Position pos) {
        log("Traveling to " + pos);
        settings.setRunning(true);
        walking.walk(pos);
    }
}